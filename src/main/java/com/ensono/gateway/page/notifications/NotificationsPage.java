package com.ensono.gateway.page.notifications;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ensono.autoframework.page.BasePage;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class NotificationsPage extends BasePage {
	
	@FindBy(id = "gatewayNotificationsPanel")
	private WebElement notificationsPanel;
	
	@FindBy(css = "md-backdrop.md-sidenav-backdrop")
	private WebElement outsideOfPanel;
	
	private NgWebDriver ngWebDriver = null;

	public NotificationsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public boolean isNotificationsPanelDisplayed() {
		return isDisplayed(notificationsPanel).booleanValue();
	}
	
	public void clickOutsideNotificationsPanel() {
		click(outsideOfPanel);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("gatewayNotificationsPanel")));
	}

}
