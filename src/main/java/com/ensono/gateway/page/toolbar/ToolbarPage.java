package com.ensono.gateway.page.toolbar;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ensono.autoframework.page.BasePage;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class ToolbarPage extends BasePage {
	
	@FindBy(css = "md-toolbar.triangular-toolbar")
	private WebElement toolbar;
	
	@FindBy(id = "gatewayNotificationsButton")
	private WebElement notificationsButton;
	
	@FindBy(id = "gatewayNotificationsPanel")
	private WebElement notificationsPanel;
	
	private NgWebDriver ngWebDriver = null;

	public ToolbarPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public void clickNotificationsButton() {
		click(notificationsButton);
		wait.until(ExpectedConditions.visibilityOf(notificationsPanel));
	}
	
	public boolean isNotificationsButtonIsClickable() {
		try {
			click(notificationsButton);
		} catch(Exception WebDriverException) {
			return false;
		}		
		return true;
	}

}
