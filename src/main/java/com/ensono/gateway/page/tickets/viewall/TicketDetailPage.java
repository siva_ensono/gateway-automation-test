package com.ensono.gateway.page.tickets.viewall;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.NoSuchElementException;

import com.ensono.gateway.page.tickets.ViewAllTicketsPage;
import com.google.common.base.Predicate;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class TicketDetailPage extends ViewAllTicketsPage {
	
	@FindBy(id = "infoDialog")
	private WebElement ticketDetailView;
	
	@FindBy(css = "#infoDialog .md-toolbar-tools h2")
	private WebElement ticketDetailViewHeader;
	
	@FindBy(css = "#infoDialog .md-toolbar-tools button")
	private WebElement ticketDetailViewCloseButton;
	
	// Other stuff
	private NgWebDriver ngWebDriver = null;
	
	public TicketDetailPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public boolean ticketDetailsViewIsDisplayed() {
		return isDisplayed(ticketDetailView).booleanValue();
	}
	
	public boolean ticketDetailsViewMatches(int ticketNumber) {
		return ticketDetailViewHeader.getText().contains(Integer.toString(ticketNumber));
	}
	
	public void closeTicketDetailsView() {
		click(ticketDetailViewCloseButton);
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("infoDialog")));
	}

}
