package com.ensono.gateway.page.tickets.viewall;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ensono.gateway.page.tickets.ViewAllTicketsPage;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class TicketsTableConfigPage extends ViewAllTicketsPage {
	
	@FindBy(id = "ticketsTableConfigDialog")
	private WebElement ticketsTableConfig;
	
	@FindBy(css = "#ticketsTableConfigDialog .md-toolbar-tools button")
	private WebElement configDialogCloseButton;
	
	@FindBy(id = "actionToolbar")
	private WebElement configActionToolbar;
	
	@FindBy(css = "#actionToolbar button[aria-label='Cancel']")
	private WebElement configCancelButton;
	
	@FindBy(css = "#actionToolbar button[aria-label='Save']")
	private WebElement configSaveButton;
	
	@FindBy(css = "#dialogContent_ticketsTableConfigDialog > md-tabs md-tab-item")
	private List<WebElement> configDialogTabs;
	
	@FindBy(css ="#actionToolbar > span")
	private WebElement resizeMessage;
	
	private NgWebDriver ngWebDriver = null;
	
	public TicketsTableConfigPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public void clickConfigTab(String tabName) {
		for (WebElement tab : configDialogTabs) {
			if (tab.getText().equalsIgnoreCase(tabName)) {
				click(tab);
				break;
			}
		}
	}
	
	public boolean configTabIsDisplayed(String tabName) {
		String tabContentId = null;
		for (WebElement tab : configDialogTabs) {
			if (tab.getText().equalsIgnoreCase(tabName)) {
				tabContentId = tab.getAttribute("aria-controls");
				break;
			}
		}
		if (tabContentId == null) {
			return false;
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(tabContentId)));
		return driver.findElement(By.id(tabContentId)).isDisplayed();
	}
	
	public void clickConfigCloseButton() {
		click(configDialogCloseButton);
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ticketsTableConfigDialog")));
	}
	
	public void clickConfigCancelButton() {
		click(configCancelButton);
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ticketsTableConfigDialog")));
	}
	
	public void clickConfigSaveButton() {
		click(configSaveButton);
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("ticketsTableConfigDialog")));
	}
	
	public boolean isResizeMessageDisplayed() {
		return isDisplayed(resizeMessage);
	}

}
