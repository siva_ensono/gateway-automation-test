package com.ensono.gateway.page.tickets.viewall.config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ensono.gateway.page.tickets.viewall.TicketsTableConfigPage;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class TicketsTableConfigDefaultSearchPatternPage extends TicketsTableConfigPage {
	
	@FindBy(id = "refreshIntervalSelect")
	private WebElement refreshIntervalSelect;

	private NgWebDriver ngWebDriver = null;
	
	public TicketsTableConfigDefaultSearchPatternPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public boolean refreshIntervalSelectIsDisplayed() {
		return isDisplayed(refreshIntervalSelect);
	}
	
	public String refreshIntervalSelectText() {
		return refreshIntervalSelect.getText();
	}
	
}