package com.ensono.gateway.page.tickets;

//import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ensono.autoframework.page.BasePage;
import com.ensono.autoframework.utils.DriverManager;
import com.ensono.autoframework.utils.ProxyServer;
import com.paulhammant.ngwebdriver.NgWebDriver;

import edu.emory.mathcs.backport.java.util.Collections;


public class ViewAllTicketsPage extends BasePage {
	
	@FindBy(id = "ticketsTable")
	private WebElement ticketsTable;
	
	@FindBy(css = ".table-header-clones")
	private WebElement ticketsTableHeader;
	
	@FindBy(id = "perPageSelect")
	private WebElement perPageSelect;
	
	@FindBy(id = "paginationButtonFirst")
	private WebElement paginationButtonFirst;
	
	@FindBy(id = "paginationButtonPrev")
	private WebElement paginationButtonPrev;
	
	@FindBy(id = "paginationButtonNext")
	private WebElement paginationButtonNext;
	
	@FindBy(id = "paginationButtonLast")
	private WebElement paginationButtonLast;
	
	@FindBy(id = "showConfigButton")
	private WebElement showConfigButton;
	
	@FindBy(id = "ticketsTableConfigDialog")
	private WebElement ticketsTableConfig;
	
	@FindBy(id = "infoDialog")
	private WebElement ticketDetails;
	
	@FindBy(css = "#infoDialog .md-toolbar-tools h2")
	private WebElement ticketDetailsHeader;
	
	// ByAngular Locators
//	private ByAngularRepeater ticketsTableColumns = ByAngular.repeater("col in vm.activeColumns");
//	private ByAngularRepeater tickets = ByAngular.repeater("ticket in vm.tickets");
//	private ByAngularRepeater perPageOptions = ByAngular.repeater("pp in vm.perPageValues");
//	private ByAngularRepeater paginationButtons = ByAngular.repeater("page in vm.pages");
	
	private By ticketsTableColumns = By.id("fake");
	private By tickets = By.id("fake2");
	private By perPageOptions = By.id("fake3");
	private By paginationButtons = By.id("fake4");
	
	// Other stuff
	private NgWebDriver ngWebDriver = null;

	public ViewAllTicketsPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public void waitForTicketsToLoad() {
		ngWebDriver.waitForAngularRequestsToFinish();
	}
	
	public boolean ticketsTableIsDisplayed() {
		return isDisplayed(ticketsTable).booleanValue();
	}
	
	public boolean ticketsTableHasAtLeastOneColumn() {
		List<WebElement> tableColumns = ticketsTableHeader.findElements(ticketsTableColumns);
		return tableColumns.size() >= 1;
	}
	
	public boolean ticketsTableUIElementsAreDisplayed() {
		return ( isDisplayed(perPageSelect).booleanValue() &&
				isDisplayed(paginationButtonFirst).booleanValue() &&
				isDisplayed(paginationButtonPrev).booleanValue() &&
				isDisplayed(paginationButtonNext).booleanValue() &&
				isDisplayed(paginationButtonLast).booleanValue() &&
				isDisplayed(showConfigButton).booleanValue() &&
				driver.findElements(tickets).size() > 0 );
	}
	
	public int sortByRandomColumn(boolean descending) {
		List<WebElement> tableColumns = ticketsTableHeader.findElements(ticketsTableColumns);
		int columnIndex = new Random().nextInt(tableColumns.size());
		WebElement columnToSortBy = tableColumns.get(columnIndex);
		if (descending) {
			click(columnToSortBy);
		}
		click(columnToSortBy);
		return columnIndex;
	}
	
	public boolean ticketsTableDataIsSortedBy(int columnIndex, boolean descending) {
		List<String> orderedTicketDataPoints = new ArrayList<String>();
		List<WebElement> ticketsData = driver.findElements(tickets);
		for (WebElement ticketDatum : ticketsData) {
			String ticketDataPoint = ticketDatum.findElements(ticketsTableColumns).get(columnIndex).getText();
			orderedTicketDataPoints.add(ticketDataPoint);
		}
		
		List<String> expected = new ArrayList<String>(orderedTicketDataPoints);
		Collections.sort(expected);
		if (descending) {
			Collections.reverse(expected);
		}
		System.out.println("orderedActual: " + orderedTicketDataPoints);
		System.out.println("expected:      " + expected);
		
		return orderedTicketDataPoints.equals(expected);		
	}
	
	public void changeRowsPerPage(int rowsPerPage) {
		click(perPageSelect);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(perPageOptions)));
		for (WebElement option : driver.findElements(perPageOptions)) {
			if (option.getText().equalsIgnoreCase(Integer.toString(rowsPerPage))) {
				click(option);
				break;
			}
		}
	}
	
	public boolean ticketsTableRowsEquals(int rowsPerPage) {
		return driver.findElements(tickets).size() == rowsPerPage;
	}
	
	public void paginateToPage(String pageNumber) {
		wait.until(ExpectedConditions.visibilityOf(paginationButtonFirst));
		List<WebElement> pages = driver.findElements(paginationButtons);
		pages.add(paginationButtonFirst);
		pages.add(paginationButtonPrev);
		pages.add(paginationButtonNext);
		pages.add(paginationButtonLast);
		for (WebElement page : pages) {
			if (page.getText().equalsIgnoreCase(pageNumber)) {
				click(page);
				break;
			}
		}
	}
	
	public List<WebElement> captureCurrentTicketsInView() {
		return driver.findElements(tickets);
	}
	
	public boolean ticketsDisplayedMatch(List<WebElement> ticketsBefore) {
		return captureCurrentTicketsInView().equals(ticketsBefore);
	}
	
	public boolean paginationButtonDisplayStatusesMatch(String listOfPages, String listOfStatuses) {
		String[] pages = listOfPages.split(",");
		String[] statuses = listOfStatuses.split(",");
		for (int i = 0; i < pages.length; i++) {
			boolean expected = Boolean.getBoolean(statuses[i]);
			boolean actual;
			
			switch (pages[i]) {
			case "first":
				actual = paginationButtonFirst.isEnabled();
			case "prev":
				actual = paginationButtonPrev.isEnabled();
			case "next":
				actual = paginationButtonNext.isEnabled();
			case "last":
				actual = paginationButtonLast.isEnabled();
			default:
				actual = expected;
			}
			
			if (actual == expected) {
				continue;
			} else {
				return false;
			}
		}
		return true;
	}
	
	public int clickRandomTicketRow() {
		List<WebElement> ticketsData = driver.findElements(tickets);
		int randomIndex = new Random().nextInt(ticketsData.size());
		WebElement randomTicket = ticketsData.get(randomIndex);
		String randomTicketNumber = ngWebDriver.retrieveAsString(randomTicket, "ticket.TicketNumber");
		
		click(randomTicket);
		return Integer.parseInt(randomTicketNumber);
	}
	
	public boolean ticketDetailsViewIsDisplayed() {
		return isDisplayed(ticketDetails).booleanValue();
	}
	
	public boolean ticketDetailsViewMatches(int ticketNumber) {
		return ticketDetailsHeader.getText().contains(Integer.toString(ticketNumber));
	}
	
	public void clickTableConfigButton() {
		click(showConfigButton);
//		wait.until(ExpectedConditions.visibilityOf(ticketsTableConfig));
	}
	
	public boolean ticketsTableConfigViewIsDisplayed() {
		return isDisplayed(ticketsTableConfig).booleanValue();
	}
	
	public List<String> getCurrentTableColumnConfig() {
		List<String> columnConfig = new ArrayList<String>();
		List<WebElement> currentColumns = ticketsTableHeader.findElements(ticketsTableColumns);
		for (WebElement column : currentColumns) {
			columnConfig.add(column.getText());
		}
		return columnConfig;
	}
	
}
