package com.ensono.gateway.page.tickets.viewall.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.ensono.gateway.page.tickets.viewall.TicketsTableConfigPage;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;
import com.paulhammant.ngwebdriver.NgWebDriver;

import edu.emory.mathcs.backport.java.util.Collections;

public class TicketsTableConfigTableLayoutPage extends TicketsTableConfigPage {
	
	@FindBy(id = "availableColumns")
	private WebElement availableColumnsList;
	
	@FindBy(id = "activeColumns")
	private WebElement activeColumnsList;
	
	@FindBy(css = ".available-column-filter input[type='text']")
	private WebElement filterTextInput;
	
	@FindBy(css = ".available-column-filter > div:not(.search-icon) button")
	private WebElement filterTextClearButton;
	
	@FindBy(css = "#activeColumns md-toast")
	private WebElement cannotRemoveColumnWarning;
	
	private ByAngularRepeater availableColumnItems = ByAngular.repeater("col in vm.filteredAvailableColumns");
	private ByAngularRepeater activeColumnItems = ByAngular.repeater("col in vm.columns.active");

	private NgWebDriver ngWebDriver = null;
	
	public TicketsTableConfigTableLayoutPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public int numberOfColumnsInGroup(String groupName) {
		if (groupName.equalsIgnoreCase("active")) {
			return activeColumnsList.findElements(activeColumnItems).size();
		} else if (groupName.equalsIgnoreCase("available")) {
			return availableColumnsList.findElements(availableColumnItems).size();
		} else {
			return 0;
		}
	}
	
	public String quickAddRandomColumn() {
		List<WebElement> availableColumns = availableColumnsList.findElements(availableColumnItems);
		WebElement randomColumn = availableColumns.get(new Random().nextInt(availableColumns.size()-1));
		String randomColumnName = randomColumn.getText();

		new Actions(driver).moveToElement(randomColumn).perform();
		WebElement quickAddButton = randomColumn.findElement(By.tagName("button"));
		click(quickAddButton);
		
		By columnXpathAvailable = By.xpath("//*[@id='availableColumns']/md-list-item/p[contains(text(), '" + randomColumnName + "')]/parent::md-list-item");
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(columnXpathAvailable));
		
		By columnXpathActive = By.xpath("//*[@id='activeColumns']/div/div/div/div/p[contains(normalize-space(text()), '" + randomColumnName + "')]/parent::div");
		wait.until(ExpectedConditions.visibilityOfElementLocated(columnXpathActive));
		
		return randomColumnName;		
	}
	
	public String quickRemoveRandomColumn() {
		List<WebElement> activeColumns = activeColumnsList.findElements(activeColumnItems);
		WebElement randomColumn = activeColumns.get(new Random().nextInt(activeColumns.size()));
		String randomColumnName = randomColumn.getText();
		
		new Actions(driver).moveToElement(randomColumn).build().perform();
		WebElement quickRemoveButton = randomColumn.findElement(By.tagName("button"));
		click(quickRemoveButton);
		
		By columnXpathActive = By.xpath("//*[@id='activeColumns']/div/div/div/div/p[contains(normalize-space(text()), '" + randomColumnName + "')]/parent::div");
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(columnXpathActive));
		
		By columnXpathAvailable = By.xpath("//*[@id='availableColumns']/md-list-item/p[contains(text(), '" + randomColumnName + "')]/parent::md-list-item");
		wait.until(ExpectedConditions.visibilityOfElementLocated(columnXpathAvailable));
		
		return randomColumnName;
	}
	
	public void moveNumberOfColumnsToGroup(int numberOfColumns, String groupName) {
		for (int i = 0; i < numberOfColumns; i++) {
			if (groupName.equalsIgnoreCase("active")) {
				quickAddRandomColumn();
			} else {
				quickRemoveRandomColumn();
			}
		}
	}
	
	public void removeColumnsFromGroup(int numberOfColumns, String groupName){
		for (int i = 0; i < numberOfColumns; i++) {
			if (groupName.equalsIgnoreCase("active")) {
				quickRemoveRandomColumn();
			} else {
				quickAddRandomColumn();
			}
		}
	}
	
	public boolean columnExistsInGroup(String columnName, String groupName) {
		By columnXpath = null;
		if (groupName.equalsIgnoreCase("active")) {
			columnXpath = By.xpath("//*[@id='" + groupName + "Columns']/div/div/div/div/p[contains(normalize-space(text()), '" + columnName + "')]/parent::div"); 
		} else {
			columnXpath = By.xpath("//*[@id='" + groupName + "Columns']/md-list-item/p[contains(normalize-space(text()), '" + columnName + "')]/parent::md-list-item");
		}
		return driver.findElement(columnXpath).isDisplayed();
	}
	
	public boolean columnOrderedByInList(String columnName, String groupName, String sortValue) {
		List<WebElement> columns = null;
		if (groupName.equalsIgnoreCase("active")) {
			columns = activeColumnsList.findElements(activeColumnItems);
		} else {
			columns = availableColumnsList.findElements(availableColumnItems);
		}
		if (sortValue.equalsIgnoreCase("on bottom")) {
			return columns.get(columns.size() - 1).getText().equalsIgnoreCase(columnName);
		} else if (sortValue.equalsIgnoreCase("alphabetically")) {
			return columnsAreSortedAlphabetically(columns);
		} else {
			return columns.get(Integer.parseInt(sortValue)).getText().equalsIgnoreCase(columnName);
		}
	}
	
	public boolean columnsAreSortedAlphabetically(List<WebElement> columns) {
		List<String> columnNames = new ArrayList<String>();
		for(WebElement column : columns) {
			columnNames.add(column.getText());
		}
		List<String> expected = new ArrayList<String>(columnNames);
		Collections.sort(expected);
		return columnNames.equals(expected);
	}
	
	public void dragAndDrop(WebElement source, WebElement target, int horizontalOffset, int verticalOffset) {
		Actions actions = new Actions(driver);
//		actions.moveToElement(source).clickAndHold().moveToElement(target).moveByOffset(0, verticalOffset).release().build().perform();
		actions.moveToElement(source).build().perform();
		actions.clickAndHold().build().perform();		
		actions.moveToElement(target).build().perform();
		actions.moveByOffset(horizontalOffset, verticalOffset).build().perform();
		actions.release().build().perform();
	}
	
	public void dragAndDrop(WebElement source, WebElement target) {
		dragAndDrop(source, target, 1, 1);
	}
	
	public String dragAndDropRandomColumn(String fromGroupName, String toGroupName) {
		List<WebElement> sourceList = null;
		WebElement source = null;
		WebElement target = null;
		
		sourceList = (fromGroupName.equalsIgnoreCase("available") ? 
				availableColumnsList.findElements(availableColumnItems) : activeColumnsList.findElements(activeColumnItems));
		
		String columnName = null;
		while(true) {
			try {
				int randomIndex = new Random().nextInt(sourceList.size());
				source = sourceList.get(randomIndex);
				columnName = source.getText();
				target = (toGroupName.equalsIgnoreCase("available") ? availableColumnsList : activeColumnsList);
				dragAndDrop(source, target);
				break;
			} catch (MoveTargetOutOfBoundsException e) {
				continue;
			}
		}
		
		By randomColumnXpath = By.xpath("//*[@id='" + fromGroupName + "Columns']/md-list-item/p[contains(text(), '" + columnName + "')]/parent::md-list-item");
		driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
//		wait.until(ExpectedConditions.invisibilityOfElementLocated(randomColumnXpath));
		
		return columnName;
	}
	
	public List<String> capturePendingColumnConfig() {
		List<String> columnConfig = new ArrayList<String>();
		List<WebElement> currentColumns = activeColumnsList.findElements(activeColumnItems);
		for (WebElement column : currentColumns) {
			columnConfig.add(column.getText());
		}
		return columnConfig;
	}
	
	public void reorderColumnsInGroup(String groupName) {
		WebElement source = null;
		WebElement target = null;
		List<WebElement> group = null;
		if (groupName.equalsIgnoreCase("active")) {
			group = activeColumnsList.findElements(activeColumnItems);
		} else {
			group = availableColumnsList.findElements(availableColumnItems);
		}
		source = group.get(0);
		target = group.get(1);
		dragAndDrop(source, target);
	}
	
	public boolean sourceIsDraggable(WebElement source) {
		Actions actions = new Actions(driver);
		actions.clickAndHold(source).moveByOffset(50, 50).build().perform();
		boolean returnValue = isVisible(By.cssSelector(".gu-mirror"));
		actions.release().build().perform();
		return returnValue;
	}
	
	public void clickRandomColumnInGroup(String groupName) {
		List<WebElement> columns = null;
		if (groupName.equalsIgnoreCase("active")) {
			columns = activeColumnsList.findElements(activeColumnItems);
		} else {
			columns = availableColumnsList.findElements(availableColumnItems);
		}
		click(columns.get(new Random().nextInt(columns.size())));
	}
	
	public boolean cannotRemoveColumnWarningIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(cannotRemoveColumnWarning));
		return isDisplayed(cannotRemoveColumnWarning);
	}
	
	public boolean cannotRemoveColumnWarningIsDraggable() {
		return sourceIsDraggable(cannotRemoveColumnWarning);
	}
	
	public boolean lastActiveColumnIsDraggable() {
		List<WebElement> columns = activeColumnsList.findElements(activeColumnItems);
		return sourceIsDraggable(columns.get(columns.size() - 1));
	}
	
	public boolean columnsHaveQuickActionButton(String groupName, String action) {	
		List<WebElement> columns = (groupName.equalsIgnoreCase("available") ? 
				availableColumnsList.findElements(availableColumnItems) : activeColumnsList.findElements(activeColumnItems));
		System.out.println("columns size: " + columns.size());
		for (WebElement column : columns) {
			try{
				if(isDisplayed(column.findElement(By.tagName("button"))).booleanValue()) {
					return true;
				}
			} catch(NoSuchElementException e) {
				// catch exception and do nothing
			}			
		}					
		return false;
	}

	private void waitForColumnsToFinishAnimating() {
		wait
		.pollingEvery(200L, TimeUnit.MILLISECONDS)
		.withTimeout(2000L, TimeUnit.MILLISECONDS)
		.until(ExpectedConditions.not(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				driver.manage().timeouts().implicitlyWait(500L, TimeUnit.MILLISECONDS);
				By animatingColumnLocator = By.cssSelector("#availableColumns .ng-animate");
				try {
					WebElement elem = driver.findElement(animatingColumnLocator);
					return true;
				} catch (Exception e) {
					return false;
				}
			}
		}));
	}
	
	public String filterAvailableColumns() {		
		List<WebElement> columns = availableColumnsList.findElements(availableColumnItems);
		String sub = null;
		while (true) {
			Random random = new Random();
			String randomColumnName = columns.get(random.nextInt(columns.size())).getText();
			int index1 = random.nextInt(randomColumnName.length());
			int index2 = random.nextInt(randomColumnName.length());
			if (index1 <= index2) {
				sub = randomColumnName.substring(index1, index2);
			} else {
				sub = randomColumnName.substring(index2, index1);
			}
			
			int count = 0;
			for (WebElement column : columns) {
				if (column.getText().contains(sub)) {
					count++;
				}
			}
			if (count > 0 && count < columns.size()) {
				break;
			} else {
				continue;
			}
		}		
		fillText(filterTextInput, sub);
		waitForColumnsToFinishAnimating();
		
		return sub;
	}
	
	public boolean availableColumnsFilteredCorrectly(String filterText) {
		List<WebElement> columns = availableColumnsList.findElements(availableColumnItems);
		for (WebElement column : columns) {
			if (!column.getText().contains(filterText)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean clearFilterTextButtonIsDisplayed() {
		return isDisplayed(filterTextClearButton);
	}
	
	public String clickClearFilterTextButton() {
		click(filterTextClearButton);
		waitForColumnsToFinishAnimating();
		return "";
	}
	
	public boolean filterTextInputIsEmpty() {
		return filterTextInput.getAttribute("value").isEmpty();
	}
	
}