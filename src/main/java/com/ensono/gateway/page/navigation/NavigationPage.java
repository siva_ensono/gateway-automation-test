package com.ensono.gateway.page.navigation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.ensono.autoframework.page.BasePage;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class NavigationPage extends BasePage{
	
	private final String navEditorSelector = "md-sidenav[md-component-id='sideNavEdit']";
	private final String modulesListSelector = navEditorSelector + " > md-content > md-list";
	private final String helperMessageSelector = ".helper-text";
	
	@FindBy(css = "button[aria-label='Home']")
	private WebElement homeLink;
	
	@FindBy(css = "md-sidenav[md-component-id='sideNavEdit']")
	private WebElement navigationEditor;
	
	@FindBy(css = "md-sidenav[md-component-id='sideNavEdit'] > div > button[aria-label='Cancel']")
	private WebElement cancelButton;
	
	@FindBy(css = "md-sidenav[md-component-id='sideNavEdit'] > div > button[aria-label='Save']")
	private WebElement saveButton;
	
	@FindBy(css = "button[aria-label='Open Navigation Edit']")
	private WebElement editNavButton;
	
	@FindBy(css = modulesListSelector)
	private WebElement modulesList;
	
	@FindBy(id = "Nav_File_Library")
	private WebElement fileLibraryLink;
	
	private ByAngularRepeater modulesSelector = ByAngular.repeater("module in vm.modules");
	private ByAngularRepeater actualModulesSelector = ByAngular.repeater("item in nav.menu");	
	private String gatewayTooltipClass = "gateway-tooltip";
	private String backdropClass = "nav-edit-backdrop";
	
	private NgWebDriver ngWebDriver;

	public NavigationPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
	}
	
	public void clickHomeLink() {
		homeLink.click();
	}
	
	public void clickFL() {
		System.out.println("clickFL page");
		click(fileLibraryLink);
		try {
			ngWebDriver.waitForAngularRequestsToFinish();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clickCancelButton() {
		waitForVisibilityOfElement(navigationEditor);
		cancelButton.click();
	}
	
	public List<String> clickSaveButton() {
		waitForVisibilityOfElement(navigationEditor);
		List<WebElement> active = findModulesByState(modulesList.findElements(modulesSelector),"active");
		List<String> moduleNames = new ArrayList<String>();
		
		for(int i=0; i<active.size(); i++) {
			String[] exploded = active.get(i).getText().split("\n");
			moduleNames.add(exploded[0]);
		}
		
		saveButton.click();
		return moduleNames;
 	}
	
	public boolean isNavigationEditorVisible() {
		try{
			waitForVisibilityOfElement(By.cssSelector(navEditorSelector));
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean isNavigationEditorInvisible() {
		try {
			waitForInVisibilityOfElement(By.cssSelector(navEditorSelector));
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public void clickManageModules() {
		waitForInVisibilityOfElement(By.cssSelector(navEditorSelector));
		editNavButton.click();
		waitForVisibilityOfElement(navigationEditor);
	}
	
	public boolean isPage(String moduleName) {
		String title = driver.getTitle();
		String[] crumbs = title.split("\\|");
		
		return moduleName.equalsIgnoreCase(crumbs[crumbs.length-1].trim());
	}
	
	public void toggleModule(String name, boolean isOn) {
		
		List<WebElement> modules = modulesList.findElements(modulesSelector);
		
		for(int i=0; i<modules.size(); i++) {
			
			if(modules.get(i).getText().equalsIgnoreCase(name)) {
				WebElement moduleSwitch = modules.get(i).findElement(By.cssSelector("md-switch"));				
				
				if(moduleSwitch.getAttribute("aria-checked") != null &&
						moduleSwitch.getAttribute("aria-checked").equalsIgnoreCase((isOn ? "false" : "true"))) {
					modules.get(i).click();
				}
			}
		}
	}
	
	
	public String disableRandomActiveModule() {
		List<WebElement> active = findModulesByState(modulesList.findElements(modulesSelector),"active");
		WebElement module = null;
		
		if(active.size()==0) {
			return "";
		} else if(active.size()==1) {
			module = active.get(0);
		} else {
			module = active.get(new Random().nextInt(active.size()-1));	
		}
		
		module.click();
		
		return module.getText();
	}
	
	public String toggleRandomModule(boolean isOn) {
		List<WebElement> modules = modulesList.findElements(modulesSelector);
		WebElement randomModule;
		
		if(modules.size()==1) {
			randomModule = modules.get(0);
		} else {
			randomModule = modules.get(new Random().nextInt(modules.size()-1));	
		}
		
		toggleModule(randomModule.getText(),isOn);
		
		return randomModule.getText();
	}
	
	public void guarenteeOneModule(String state) {
		List<WebElement> active = findModulesByState(modulesList.findElements(modulesSelector),state);
		
		if(active.size()==0) {
			toggleRandomModule(state.equalsIgnoreCase("active"));
		}
	}
	
	public boolean matchActiveModules(List<String> active) {
		List<WebElement> actualModules = driver.findElements(actualModulesSelector);
		
		for(int i=0; i<actualModules.size(); i++) {
			if(actualModules.get(i).getText().equalsIgnoreCase("Home")) {
				actualModules.remove(i);
				i--;
			}
		}
		
		if(active.size() != actualModules.size()) {
			return false;
		}
		
		for(int i=0; i<actualModules.size(); i++) {
			if(!actualModules.get(i).getText().equalsIgnoreCase(active.get(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean isReEnableMessageVisible(String moduleName) {
		List<WebElement> modules = modulesList.findElements(modulesSelector);
		WebElement helper = null;
		
		for(int i=0; i<modules.size(); i++) {
			String[] exploded = modules.get(i).getText().split("\n");
			if(exploded[0].equalsIgnoreCase(moduleName)) {
				helper = modules.get(i).findElement(By.cssSelector(helperMessageSelector));
			}
		}
		try{
			return helper.isDisplayed();
		} catch(Exception e) {
			return false;
		}
	}
	
	public boolean isTooltipVisible(boolean visible) {
		if(visible) {
			waitForVisibilityOfElement(By.className(gatewayTooltipClass));		
		} else {
			waitForInVisibilityOfElement(By.className(gatewayTooltipClass));
		}
		return isDisplayed(By.className(gatewayTooltipClass));
	}
	
	public boolean isBackdropVisible() {
		waitForVisibilityOfElement(By.className(backdropClass));
		return isDisplayed(By.className(backdropClass));
	}
	
	private List<WebElement> findModulesByState(List<WebElement> modules, String state) {
		List<WebElement> activeModules = new ArrayList<WebElement>();
		for(int i=0; i<modules.size(); i++) {
			WebElement moduleSwitch = modules.get(i).findElement(By.cssSelector("md-switch"));
			if(moduleSwitch.getAttribute("aria-checked") != null &&
					moduleSwitch.getAttribute("aria-checked").equalsIgnoreCase(state.equalsIgnoreCase("active") ? "true" : "false")) {
				activeModules.add(modules.get(i));
			}
		}
		return activeModules;
	}
}
