package com.ensono.gateway.page.fileLibrary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.ensono.autoframework.page.BasePage;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;
import com.paulhammant.ngwebdriver.NgWebDriver;


public class FileLibraryPage extends BasePage{
	
	@FindBy(className = "files-loading")
	private WebElement filesLoadingElement;
	
	private ByAngularRepeater folderSelector = ByAngular.repeater("item in item.nodes");
	private ByAngularRepeater filesAreaSelector = ByAngular.repeater("file in vm.files");
	private ByAngularRepeater breadcrumbsSelector = ByAngular.repeater("bc in vm.breadcrumbs");
	
	NgWebDriver ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
	

	public static final String FOLDER_ID = "FolderId";
	public static final String FOLDER_NAME = "FolderName";
	

	public FileLibraryPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}	
	

	public Map<String, Object> openRandomFolderFromTree() {
		List<WebElement> folderElements = driver.findElements(folderSelector);		
		Iterator<WebElement> folderIterator = folderElements.iterator();
		Map<String, Object> folderDetailsMap = null;		

		while(folderIterator.hasNext()) {
			WebElement element = folderIterator.next();
			if(element.getText().trim().isEmpty()) {
				folderIterator.remove();
			}
		}		

		try {
			WebElement randomFolderElement = folderElements.get(new Random().nextInt(folderElements.size()));
			int folderId = Integer.parseInt(ngWebDriver.retrieveJson(randomFolderElement, "item.id"));
            String folderName = ngWebDriver.retrieveAsString(randomFolderElement, "item.name");
			click(randomFolderElement);
			waitForInvisibility(filesLoadingElement, 10);
			folderDetailsMap = new HashMap<>();
			folderDetailsMap.put(FOLDER_NAME, folderName);
			folderDetailsMap.put(FOLDER_ID, folderId);
		} catch(Exception e) {
			e.printStackTrace();
		}		
		return folderDetailsMap;
	}
	
	public Map<String, Object> openRandomFolderFromList() {
        List<WebElement> folderElements = driver.findElements(filesAreaSelector);        
        Iterator<WebElement> folderIterator = folderElements.iterator();
        String foldertype = null; 
        Map<String, Object> folderDetailsMap = null;     
        
        Actions action = new Actions(driver);

        while(folderIterator.hasNext()) {
               WebElement element = folderIterator.next();
               foldertype = element.findElements(By.tagName("span")).get(2).getText();
               if(!foldertype.equals("folder")) {
                      folderIterator.remove();
               }
        }

        try {	
            WebElement randomFolderElement = folderElements.get(new Random().nextInt(folderElements.size()));
            String folderName = ngWebDriver.retrieveAsString(randomFolderElement, "file.name");
            int folderId = Integer.parseInt(ngWebDriver.retrieveJson(randomFolderElement, "file.id"));
            action.doubleClick(randomFolderElement).perform();
            waitForInvisibility(filesLoadingElement, 10);
            
            folderDetailsMap = new HashMap<>();
            folderDetailsMap.put(FOLDER_NAME, folderName);
            folderDetailsMap.put(FOLDER_ID, folderId);
        } catch(Exception e) {
               e.printStackTrace();
        }            
        
        return folderDetailsMap;
  }
	
	public boolean isFolderSelectedInTree(int selectedFolderId)  {
		List<WebElement> folderElements = driver.findElements(folderSelector);
		Iterator<WebElement> folderIterator = folderElements.iterator();		
		int folderId;
		
		while(folderIterator.hasNext()) {
			WebElement element = folderIterator.next();
			folderId = Integer.parseInt(ngWebDriver.retrieveJson(element, "item.id"));
			if(folderId == selectedFolderId) {
				return element.isSelected();
			}
		}		
		
		return false;
	}
	
	public List<String> getFolderContents() {
		List<String> folderContents = null;
		try{
			List<WebElement> folderContentElements = driver.findElements(filesAreaSelector);
			folderContents = new ArrayList<>();
			for (WebElement element : folderContentElements) {
				folderContents.add(element.findElements(By.tagName("span")).get(0).getText());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return folderContents;
		
	}
	
	public List<String> getBreadcrumbs() {
		List<WebElement> breadcrumbElements = driver.findElements(breadcrumbsSelector);
		List<String> breadcrumbs = new ArrayList<>();
		
		for (WebElement element : breadcrumbElements) {
			breadcrumbs.add(element.getText());
		}
		
		return breadcrumbs;
	}
	
	public Map<String, Object> clickRandomBreadcrumb() {
		List<WebElement> bc = driver.findElements(breadcrumbsSelector);
		Map<String, Object> folderDetails = null;
		Integer selectedfolderId = null;
		
		WebElement randomElement = bc.get(new Random().nextInt(bc.size()-1));			
		
		try{
			String folderId = ngWebDriver.retrieveJson(randomElement, "bc.id");				
			
			if(!folderId.equals("null")) {					
				selectedfolderId = Integer.parseInt(folderId);
			}
			else
			{
				selectedfolderId = 0;
			}				
			String selectedFolderName = randomElement.getText();
			click(randomElement);
			waitForInvisibility(filesLoadingElement, 10);				
			folderDetails = new HashMap<>();
			folderDetails.put(FOLDER_ID, selectedfolderId);
			folderDetails.put(FOLDER_NAME, selectedFolderName);				
			return folderDetails;
		} catch(Exception e) {				
			e.printStackTrace();
		}
		return folderDetails;
	}

}
