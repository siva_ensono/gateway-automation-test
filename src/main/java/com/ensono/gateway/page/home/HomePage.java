package com.ensono.gateway.page.home;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.ensono.autoframework.page.BasePage;
import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.ByAngularRepeater;
import com.paulhammant.ngwebdriver.NgWebDriver;

public class HomePage extends BasePage {
	
	private NgWebDriver ngWebDriver = null;
	
	private ByAngularRepeater crumbs = ByAngular.repeater("crumb in vm.breadcrumbs.crumbs");

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) driver);
		driver.manage().timeouts().setScriptTimeout(5000, TimeUnit.MILLISECONDS);
	}
	
	public boolean pageTitleMatches(String title) {
		waitForVisibilityOfElement(crumbs.row(0));
		WebElement pageTitle = driver.findElement(crumbs.row(0));
		System.out.println(pageTitle.getText());
		return pageTitle.getText().equals(title);
	}
}
