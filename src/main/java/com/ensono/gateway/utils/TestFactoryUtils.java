package com.ensono.gateway.utils;

import java.util.HashMap;
import java.util.List;

public class TestFactoryUtils {
	
	private static HashMap<String, Object> testFactory = null;
	
	private static HashMap<String, Object> getTestFactory() {
		if (testFactory == null) {
			setTestFactory(new HashMap<String, Object>());
		}
		return testFactory;
	}
	
	private static void setTestFactory(HashMap<String, Object> factory) {
		testFactory = (factory);
	}
	
	public static Object getTestObject(String objectKey) throws Exception {
		HashMap<String, Object> factory = getTestFactory();
		if (!factory.containsKey(objectKey)) {
			throw new Exception("Could not find object key: " + objectKey + " in testFactory. Please specify a valid key.");
		}
		return factory.get(objectKey);
	}
	
	public static Integer getIntegerTestObject(String objectKey) throws Exception {
		try {
			Object obj = getTestObject(objectKey);
			if (obj instanceof Integer) {
				return (Integer) obj;
			} else {
				throw new Exception("Object for objectKey " + objectKey + " is not an Integer. Please use the correct getter type method.");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static String getStringTestObject(String objectKey) throws Exception {
		try {
			Object obj = getTestObject(objectKey);
			if (obj instanceof String) {
				return (String) obj;
			} else {
				throw new Exception("Object for objectKey " + objectKey + " is not a String. Please use the correct getter type method.");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static List<String> getListStringTestObject(String objectKey) throws Exception {
		try {
			Object obj = getTestObject(objectKey);
			if (obj instanceof List) {
				return (List<String>) obj;
			} else {
				throw new Exception("Object for objectKey " + objectKey + " is not a List<String>. Please use the correct getter type method.");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static void storeTestObject(String objectKey, Object object) throws Exception {
		HashMap<String, Object> factory = getTestFactory();
		if (objectKey == null || objectKey.isEmpty()) {
			throw new Exception("Please define an object key to store your object in testFactory.");
		}
		if (object == null) {
			throw new Exception("Cannot store a null object in testFactory. Please define an object to store with objectKey " + objectKey);
		}
		factory.put(objectKey, object);
	}
	
}
