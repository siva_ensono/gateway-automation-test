package com.ensono.gateway.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;


public class ApiCall {

	public JSONArray getApiResponse(String folderId) throws Exception {
		try {
			URL apiURL = new URL("https://vbahhopka9.execute-api.us-west-2.amazonaws.com/FLCERT/folder/" + folderId);
			HttpURLConnection conn = (HttpURLConnection) apiURL.openConnection();
	        conn.setRequestMethod("GET");           
	        conn.setRequestProperty("Accept", "application/json");
	           
	        if (conn.getResponseCode() != 200) {
	        	throw new RuntimeException(" HTTP error code : " + conn.getResponseCode());
	        }
	        Scanner scan = new Scanner(apiURL.openStream());
	        String entireResponse = new String();
	        while (scan.hasNext()) entireResponse += scan.nextLine();
	           
	        entireResponse = "{\"response\":" + entireResponse + "}";
	        scan.close();
	        conn.disconnect();
	           
	        JSONObject obj = new JSONObject(entireResponse);
	        JSONArray arr = obj.getJSONArray("response");
	           
	        if(arr.length() > 0) {
	        	return arr;
	        }
	           
	     } catch (MalformedURLException e) {
	    	 e.printStackTrace();
	     } catch (IOException e) {
	    	 e.printStackTrace();
	     }
		return null;
	}
	
	public List<String> getFolderNames(String folderId) throws Exception {
		JSONArray arr = getApiResponse(folderId);
		List<String> folders = null;
		
		if(arr != null && arr.length() > 0) {
			folders = new ArrayList<>();
			for (int i = 0; i < arr.length(); i++) {
	        	folders.add(arr.getJSONObject(i).getString("name"));
	        }
		}
        return folders;	
	}
}
