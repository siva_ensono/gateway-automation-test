@all
Feature: Home
	As a Gateway user
	I want to be shown a landing page when I log into my gateway
	So that a common starting point is established for each user
	
Background:
	Given the gateway application is loaded

Scenario: Landing page exists
	Then I should be on the 'Home' page
	
	
Scenario: Click on home navigation link
	Given I am on the view all tickets page
	When I click on the home navigation link
	Then I should be on the 'Home' page
	