@NavigationEditor @all
Feature: NavigationEditor
As a gateway user
I want the ability to configure the modules on my gateway experience
So that I only see the information that is important to me

Background:
	Given the gateway application is loaded

@GTWY-41
Scenario: Navigation editor opens
	When I click the Manage Modules button
	Then the navigation editor will appear
	
@GTWY-41
Scenario: Navigation editor closes
	Given I click the Manage Modules button
	When I click the cancel button
	Then the navigation editor will disappear
	
@GTWY-42
Scenario: disable active modules
	Given I click the Manage Modules button
	And there is at least one module 'active'
	When I 'disable' a random module
	And I click the save button
	Then the module will not show up in the navigation sidebar
	
@GTWY-42
Scenario: enable inactive module
	Given I click the Manage Modules button
	And there is at least one module 'inactive'
	When I 'enable' a random module
	And I click the save button
	Then the module will show up in the navigation sidebar
	
@GTWY-42
Scenario: redirect to home
	Given I am on the tickets module
	And I click the Manage Modules button
	When I disable the 'Ticketing' module
	And I click the save button
	Then I will be redirected to the home page
	
@GTWY-78
Scenario: restore message visible
	Given I click the Manage Modules button
	And I disable the 'Ticketing' module
	And I click the save button
	When I click the Manage Modules button
	And I enable the 'Ticketing' module
	Then the tooltip should be visible
	
@GTWY-78
Scenario: restore message not visible
	Given I click the Manage Modules button
	And I disable the 'Ticketing' module
	And I click the save button
	When I click the Manage Modules button
	And I enable the 'Ticketing' module
	And I disable the 'Ticketing' module
	Then the tooltip should not be visible	
	
@GTWY-78
Scenario: restore message not visible
	Given I click the Manage Modules button
	And I enable the 'Ticketing' module
	And I click the save button
	When I click the Manage Modules button
	And I disable the 'Ticketing' module
	And I enable the 'Ticketing' module
	Then the tooltip should not be visible
	
@GTWY-78
Scenario: backdrop exists
	Given I click the Manage Modules button
	Then the backdrop should be visible
	
@GTWY-78
Scenario: elements underneath backdrop are not clickable
	Given I click the Manage Modules button
	Then notification button is not clickable