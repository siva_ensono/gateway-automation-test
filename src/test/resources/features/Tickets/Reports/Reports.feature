@tickets @all
Feature: Reports view should exist
	As a Gateway user
	I want to access my tickets graphs and tickets table in different locations
	So that performance of the tickets table is speedy and the separation of the table and graphs logically bunches information together


@sanity @GTWY-19
Scenario: Reports view is displayed
	Given I am a person
	When I visit the url '#/tickets/reports'
	Then the 'Reports' page is loaded