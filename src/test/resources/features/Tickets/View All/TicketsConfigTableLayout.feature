@Tickets @ViewAllTickets @TicketsTableConfig @TicketsTableConfigTableLayout @all
Feature: Tickets Table Layout Config
	As a tickets table user
	I want to configure my table layout
	So that I can fit the tickets table to my needs
	

Background:
	Given I am on the view all tickets page
	And I click on the tickets table configuration button
	And I click on the 'Table Layout' config tab
	
	
@GTWY-15
Scenario Outline: Quick add and quick remove columns
	Given there is at least '<number>' columns in '<from_group_name>'
	When I click the quick '<action>' button on a random column in '<from_group_name>'
	Then that column is moved to '<to_group_name>'
	And that column is sorted '<sort_placement>' in group '<to_group_name>'
Examples:
	| number | action | from_group_name | to_group_name | sort_placement |
	| 1      | add    | available       | active        | on bottom      |
	| 2      | remove | active          | available     | alphabetically |
	
	
@GTWY-15 @GTWY-45
Scenario Outline: Drag and drop columns
	Given there is at least '<number>' columns in '<from_group_name>'
	When I drag and drop a random column from '<from_group_name>' into '<to_group_name>'
	Then that column is moved to '<to_group_name>'
Examples:
	| number | from_group_name | to_group_name | sort_placement |
	| 1      | available       | active        | on bottom      |
	| 2      | active          | available     | alphabetically |
	
	
@GTWY-45
 Scenario: Active columns can be no more than 10
   Given there are '10' columns in 'active'
   When I drag and drop a random column from 'available' into 'active'
   Then the cannot have more than 10 active columns warning will appear
   And the columns in 'available' does not have a quick 'add' button

@GTWY-15
Scenario Outline: Save, Cancel, and Close on changing layout should work properly
	Given I change the table layout column configuration
	When I click the tickets table config '<action>' button
	Then the tickets table configuration view is hidden 
	And the tickets table has at least one column
	And the tickets table configuration '<match_status>'
Examples:
	| action | match_status   |
	| save   | matches        |
	| cancel | does not match |
	| close  | does not match |


@GTWY-15 @GTWY-45
Scenario: Columns in active list can be reordered
	Given there is at least '2' columns in 'active'
	When I reorder the columns in 'active'
	And I click the tickets table config 'save' button
	Then the tickets table configuration 'matches'
	

@GTWY-15 @GTWY-45
Scenario: Last column cannot be removed from active, message appears
    Given I move all but one column to available
    When I click on the only column in active
    Then the cannot remove column warning will appear
    And the cannot remove column warning is not draggable
    And the only column in active is not draggable
    And the column in 'active' does not have a quick 'remove' button
	
@GTWY-44
Scenario: Columns can be filtered by text
	Given there is at least '2' columns in 'available'
	When I filter the available columns
	Then the available columns are filtered correctly
	And the clear filter text button is displayed
	

@GTWY-44
Scenario: Column Filter text can be cleared
	Given there is at least '2' columns in 'available'
	And I filter the available columns
	When I click the clear filter text button
	Then the available columns are filtered correctly
	And the clear filter text button is hidden
	And the filter text input is empty
	
@GTWY-44
Scenario: Filtered columns can be dragged into active
	Given there is at least '2' columns in 'available'
	And I filter the available columns
	When I drag and drop a random column from 'available' into 'active'
	Then that column is moved to 'active'
	
	
#@GTWY-44
#Scenario Outline: Active columns dragged back into available abide by filter rules
#	Given there is at least '2' columns in 'available'
#	And I filter the available columns