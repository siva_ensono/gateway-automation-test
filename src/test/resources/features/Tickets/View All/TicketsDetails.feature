@Tickets @ViewAllTickets @TicketDetails @all
Feature: TicketDetails
	As a tickets table viewer
	I want a detailed view for and individual ticket
	So that I can take more refined action on or get more refined detail about a ticket
	
Background:
	Given I am on the view all tickets page
	And I click a random ticket
	
	
@GTWY-11
Scenario: Ticket details view closes when you click the X button
	Given the ticket details view is displayed
	When I click the ticket details view close button
	Then the ticket details view is closed