@Tickets @ViewAllTickets @TicketsTableConfig @all
Feature: TicketsConfig
	As a tickets table viewer
	I want to configure my tickets table view
	So that I can take make tickets table interaction fit my needs
	
Background:
	Given I am on the view all tickets page
	And I click on the tickets table configuration button
	
	
@GTWY-9 @GTWY-34
Scenario Outline: Clicking on tabs switches between config views
	Given the tickets table configuration view is displayed
	When I click on the '<config_tab_name>' config tab
	Then the '<config_tab_name>' tab is displayed
Examples:
	| config_tab_name          |
	| Table Layout             |
	| Ticket System Connection |
	| Other Config             |
	| Default Search Pattern   |
	
@GTWY-45
Scenario: Resize Message displays on Tickets Table Layout Configuration
	Given the tickets table configuration view is displayed
	When I click on the 'Table Layout' config tab
	Then the resize message is displayed