@Tickets @ViewAllTickets @TicketsTableConfig @TicketsTableConfigDefaultSearchPattern @all
Feature: Tickets Table Default Search Pattern
	As a tickets table user
	I want to configure my def search pattern
	So that I can be 1337 as can be
	

Background:
	Given I am on the view all tickets page
	And I click on the tickets table configuration button
	And I click on the 'Default Search Pattern' config tab
	
	
@GTWY-13
Scenario: Refresh Rate defaults to 60 seconds
	And the refresh rate selector is displayed
	And the refresh rate selector is defaulted to 60 seconds