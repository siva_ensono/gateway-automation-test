@Tickets @ViewAllTickets @all
Feature: Tickets
	As a Gateway User
	I want to view my ticket data in a table
	So that I can interact with my tickets
	


@GTWY-20
Scenario: Tickets table displays with all UI elements and at least one column
	Given I am on the view all tickets page
	When I examine the tickets table
	Then the tickets table has at least one column
	And all tickets table UI elements are displayed


@GTWY-21
Scenario Outline: Column Headers Sort Data properly
	Given I am on the view all tickets page
	When I sort by random ticket column '<direction>'
	Then the data is ordered by that column '<direction>'
Examples:
	| direction |
	| ASC       |
	| DESC      |
	

@GTWY-21
Scenario Outline: Rows per page filters results
	Given I am on the view all tickets page
	When I change the rows per page to '<rows_per_page>'
	Then '<rows_per_page>' rows are displayed in the tickets table
Examples:
	| rows_per_page |
	| 25            |
	| 50            |
	| 100           |
	

@GTWY-21
Scenario Outline: Tickets Table Pagination
	Given I am on the view all tickets page
	And I am on table page '<page_number>'
	When I click the pagination button labeled '<new_page>'
	Then the table rows displayed should be different
	And the enabled status of '<list_of_pages>' should be '<list_of_statuses>'
Examples:
  | page_number | new_page | list_of_page_buttons | list_of_statuses      |
  | 1           | next     | first,prev,next,last | true,true,true,true   |
  | 1           | last     | first,prev,next,last | true,true,false,false |
  | 3           | prev     | first,prev,next,last | true,true,true,true   |
  | 3           | first    | first,prev,next,last | false,false,true,true |
  | 3           | 2        | first,prev,next,last | true,true,true,true   |


@GTWY-11
Scenario: Ticket Details displays when you click a ticket row
	Given I am on the view all tickets page
	When I click a random ticket
	Then the ticket details view is displayed
	And the ticket details view matches that ticket
	
	
@GTWY-9
Scenario: Tickets Table Configuration view displays
	Given I am on the view all tickets page
	When I click on the tickets table configuration button
	Then the tickets table configuration view is displayed
	