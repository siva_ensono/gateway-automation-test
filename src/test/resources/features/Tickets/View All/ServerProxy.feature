@Proxy @all
Feature: Server Proxy
	As a Gateway Tester
	I want to add proxy listeners
	So that I can filter responses

@GTWY-16
Scenario Outline: Add proxy listeners
	Given http request is intercepted
	When request end point matches with '<request_interceptor>' 
	Then response received with '<message_contents>'
Examples:
|  request_interceptor               |	message_contents      						|  
|  /tickets-table-config.tmpl.html   |  This message body will appear in responses! |
  
