@Notifications @all
Feature: Notifications
As a gateway user
I want a notifications quick view panel
So that I can view relevant information to my current or past sessions

Background:
	Given the gateway application is loaded

@GTWY-39
Scenario: Notifications Panel Opens
	When I click notifications button
	Then the notifications panel is displayed
	
@GTWY-39
Scenario: Notifications Panel Closes when clicking outside
	Given I click notifications button
	When I click outside the notifications panel
	Then the notifications panel is closed