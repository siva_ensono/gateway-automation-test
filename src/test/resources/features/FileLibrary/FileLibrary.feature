@FileLibrary @all
Feature: File Library

As a gateway user
I want the file library module
So that I will be able to upload/view/download files

Background: 
	Given the FileLibrary module is opened
	

@GTWY-131
Scenario Outline: Folder contents
	When I open a random folder from '<FLView>'
	Then I see the contents of the folder
Examples:
	| FLView     |
	| tree       |
	| list       |
	

@GTWY-72
Scenario: Breadcrumbs are clickable
    Given I open a random folder from 'tree'
    When I click a breadcrumb link
    Then I see the contents of the folder
