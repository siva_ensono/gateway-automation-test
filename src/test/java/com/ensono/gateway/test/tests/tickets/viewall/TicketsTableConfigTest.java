package com.ensono.gateway.test.tests.tickets.viewall;

import java.util.List;

import org.testng.Assert;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.tickets.viewall.TicketsTableConfigPage;
import com.ensono.gateway.page.tickets.viewall.config.TicketsTableConfigTableLayoutPage;
import com.ensono.gateway.utils.TestFactoryUtils;

public class TicketsTableConfigTest {

	private TicketsTableConfigPage ticketsTableConfigPage = new TicketsTableConfigPage(DriverManager.getDriver());
	
	public void clickOnConfigTab(String tabName) {
		ticketsTableConfigPage.clickConfigTab(tabName);
	}
	
	public void verifyConfigTabIsDisplayed(String tabName) {
		Assert.assertTrue(ticketsTableConfigPage.configTabIsDisplayed(tabName));
	}
	
	public void clickConfigDialogActionButton(String buttonType) {
		try {
			TicketsTableConfigTableLayoutPage ticketsTableConfigTableLayoutPage = new TicketsTableConfigTableLayoutPage(DriverManager.getDriver());
			List<String> config = ticketsTableConfigTableLayoutPage.capturePendingColumnConfig();
			TestFactoryUtils.storeTestObject("CURRENT_COLUMN_CONFIG", config);
			if (buttonType.equalsIgnoreCase("save")) {
				ticketsTableConfigPage.clickConfigSaveButton();
			} else if (buttonType.equalsIgnoreCase("cancel")) {
				ticketsTableConfigPage.clickConfigCancelButton();
			} else if (buttonType.equalsIgnoreCase("close")) {
				ticketsTableConfigPage.clickConfigCloseButton();
			} else {
				Assert.fail();
			}
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	public void verifyResizeMessageDisplayed() {
		Assert.assertTrue(ticketsTableConfigPage.isResizeMessageDisplayed());
	}
	
}
