package com.ensono.gateway.test.tests.tickets.viewall.config;

import org.testng.Assert;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.tickets.viewall.config.TicketsTableConfigDefaultSearchPatternPage;

public class TicketsTableConfigDefaultSearchPatternTest {
	
	private TicketsTableConfigDefaultSearchPatternPage ticketsTableConfigDefaultSearchPatternPage = new TicketsTableConfigDefaultSearchPatternPage(DriverManager.getDriver());
	
	public void checkRefreshIntervalSelectorIsDisplayed() {
		Assert.assertTrue(ticketsTableConfigDefaultSearchPatternPage.refreshIntervalSelectIsDisplayed());
	}
	
	public void checkRefreshDefaultsToSixty() {
		Assert.assertEquals("60", ticketsTableConfigDefaultSearchPatternPage.refreshIntervalSelectText());
	}

}
