package com.ensono.gateway.test.tests.tickets.viewall;

import org.testng.Assert;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.tickets.viewall.TicketDetailPage;
import com.ensono.gateway.utils.TestFactoryUtils;

public class TicketDetailsTest {

	private TicketDetailPage ticketDetailPage = new TicketDetailPage(DriverManager.getDriver());
	
	public void verifyTicketDetailsViewIsDisplayed() {
		Assert.assertTrue(ticketDetailPage.ticketDetailsViewIsDisplayed());
	}
	
	public void verifyTicketDetailsNumberMatches() {
		try {
			int randomTicketNumber = TestFactoryUtils.getIntegerTestObject("RANDOM_TICKET_NUMBER");
			Assert.assertTrue(ticketDetailPage.ticketDetailsViewMatches(randomTicketNumber));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void closeTicketsDetailView() {
		ticketDetailPage.closeTicketDetailsView();
	}
	
	public void verifyTicketDetailsViewIsClosed() {
		Assert.assertFalse(ticketDetailPage.ticketDetailsViewIsDisplayed());
	}
	
}
