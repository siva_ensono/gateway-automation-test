package com.ensono.gateway.test.tests.toolbar;

import org.testng.Assert;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.toolbar.ToolbarPage;

public class ToolbarTest {

	ToolbarPage toolbarPage = new ToolbarPage(DriverManager.getDriver());
	
	public void clickNotificationsButton() {
		toolbarPage.clickNotificationsButton();
	}
	
	public void verifyNotificationsButtonIsNotClickable() {
		Assert.assertFalse(toolbarPage.isNotificationsButtonIsClickable());
	}
	
}
