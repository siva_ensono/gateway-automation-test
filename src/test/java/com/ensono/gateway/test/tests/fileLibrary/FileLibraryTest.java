package com.ensono.gateway.test.tests.fileLibrary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.testng.Assert;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.fileLibrary.FileLibraryPage;
import com.ensono.gateway.page.navigation.NavigationPage;
import com.ensono.gateway.utils.ApiCall;
import com.ensono.gateway.utils.TestFactoryUtils;

public class FileLibraryTest {
	
	FileLibraryPage fileLibraryPage = new FileLibraryPage(DriverManager.getDriver());
	NavigationPage navigationPage = new NavigationPage(DriverManager.getDriver());
	ApiCall apiCall = new ApiCall();
	
	public void openFileLibrary() {
		navigationPage.clickFL();
	}
	
	public void openRandomFolderFromTree() {
		Map<String, Object> selectedFolderDetails = fileLibraryPage.openRandomFolderFromTree();
		validateFolderOpened(selectedFolderDetails);	
	}
	
	public void openRandomFolderFromList() {
		Map<String, Object> selectedFolderDetails = fileLibraryPage.openRandomFolderFromList();
		validateFolderOpened(selectedFolderDetails);
	}
	
	private void validateFolderOpened(Map<String, Object> selectedFolderDetails) {
		String selectedFolder = (String) selectedFolderDetails.get(FileLibraryPage.FOLDER_NAME);
		int selectedFolderId = (Integer) selectedFolderDetails.get(FileLibraryPage.FOLDER_ID);
		
		try {
			TestFactoryUtils.storeTestObject(FileLibraryPage.FOLDER_ID, selectedFolderId);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		List<String> breadcrumbs = fileLibraryPage.getBreadcrumbs();
		String lastBreadcrumb = breadcrumbs.get(breadcrumbs.size()-1);
		Assert.assertEquals(selectedFolder, lastBreadcrumb);
	}
	
	public void verifyFolderContents() {
		List<String> folderNames = new ArrayList<>();
		Integer selectedFolderId;
		try {
			selectedFolderId = TestFactoryUtils.getIntegerTestObject(FileLibraryPage.FOLDER_ID);			
			if(selectedFolderId == 0) {
				folderNames = apiCall.getFolderNames("null");
			} else {
				folderNames = apiCall.getFolderNames(selectedFolderId.toString());
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		Assert.assertEquals(folderNames, fileLibraryPage.getFolderContents());
	}

	public void openRandomFolderFromBreadcrumbs() {
		Integer folderid = (Integer) fileLibraryPage.clickRandomBreadcrumb().get(FileLibraryPage.FOLDER_ID);
		try {
			TestFactoryUtils.storeTestObject(FileLibraryPage.FOLDER_ID, folderid);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
