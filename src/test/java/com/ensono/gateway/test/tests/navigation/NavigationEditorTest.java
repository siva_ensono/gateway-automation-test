package com.ensono.gateway.test.tests.navigation;

import java.util.List;

import org.testng.Assert;

import com.ensono.autoframework.utils.ConfigUtil;
import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.navigation.NavigationPage;
import com.ensono.gateway.utils.TestFactoryUtils;


public class NavigationEditorTest {
	
	NavigationPage navPage = new NavigationPage(DriverManager.getDriver());
	
	final String activeModulesKey = "ACTIVE_MODULES";
	final String selectedModuleKey = "SELECTED_MODULE";
	
	public void clickManageModules() {
		navPage.clickManageModules();
	}
	
	public void clickCancelButton() {
		navPage.clickCancelButton();
	}
	
	public void disableModule(String module) {
		navPage.toggleModule(module, false);
	}
	
	public void enableModule(String module) {
		navPage.toggleModule(module, true);
	}
	
	public void verifyCurrentPage(String moduleName) {
		Assert.assertTrue(navPage.isPage(moduleName));
	}
	
	public void loadTicketsModule() {
		navPage.loadPage(ConfigUtil.getInstance().getConfig().getURL() + "#/tickets/view");
	}
	
	public void clickSaveButton() {
		List<String> activeModules = navPage.clickSaveButton();
		try{
			TestFactoryUtils.storeTestObject(activeModulesKey, activeModules);
		} catch(Exception e) {
			Assert.fail();
		}
	}
	
    public void verifyEditorIsVisible(boolean isVisible) {
        if(isVisible) {
            Assert.assertTrue(navPage.isNavigationEditorVisible());
        } else {
            Assert.assertTrue(navPage.isNavigationEditorInvisible());
        }
    }
    
    public void toggleRandomModule(String action) {
    	String module = navPage.toggleRandomModule(action.equalsIgnoreCase("enable"));
    	try { 
    		TestFactoryUtils.storeTestObject(selectedModuleKey, module);
    	} catch(Exception e) {
    		Assert.fail();
    	}
    }
    
    public void disableRandomActiveModule() {
    	String module = navPage.disableRandomActiveModule();
    	try{
    		TestFactoryUtils.storeTestObject(selectedModuleKey, module);
    	} catch(Exception e) {
    		Assert.fail();
    	}
    }

    public void guarenteeOneModule(String state) {
    	navPage.guarenteeOneModule(state);
    	clickSaveButton();
    	clickManageModules();
    }
    
    public void verifyActiveModules() {
    	try {
    		Assert.assertTrue(navPage.matchActiveModules(TestFactoryUtils.getListStringTestObject(activeModulesKey)));
    	} catch(Exception e) {
    		e.printStackTrace();
    		Assert.fail();
    	}
    }
    
    public void verifyTooltipIsVisible() {
    	Assert.assertTrue(navPage.isTooltipVisible(true));
    }
    
    public void verifyTooltipIsInvisible() {
    	Assert.assertFalse(navPage.isTooltipVisible(false));
    }
    
    public void verifyBackdropIsVisible() {
    	Assert.assertTrue(navPage.isBackdropVisible());
    }
}
