package com.ensono.gateway.test.tests.notifications;

import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.notifications.NotificationsPage;

import junit.framework.Assert;

public class NotificationsTest {
	
	NotificationsPage notificationsPage = new NotificationsPage(DriverManager.getDriver());
	
	public void clickOutsideNotificationsPanel() {
		notificationsPage.clickOutsideNotificationsPanel();
	}
	
	public void isNotificationPanelDisplayed(boolean displayed) {
		Assert.assertEquals(displayed, notificationsPage.isNotificationsPanelDisplayed());
	}

}
