package com.ensono.gateway.test.tests.tickets;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.ensono.autoframework.utils.ConfigUtil;
import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.tickets.ViewAllTicketsPage;
import com.ensono.gateway.page.tickets.viewall.TicketDetailPage;
import com.ensono.gateway.utils.TestFactoryUtils;

public class ViewAllTicketsTest {
	
	private ViewAllTicketsPage viewAllTicketsPage;
	private TicketDetailPage ticketDetailPage;
	private String VIEW_ALL_TICKETS_PAGE_URL_PART = "#/tickets/view";
	private String COLUMN_SORT_DIRECTION_ASC = "ASC";
	private String COLUMN_SORT_DIRECTION_DESC = "DESC";
	private int sortingColumnIndex;
	private List<WebElement> currentTicketsInView;

	public void loadViewAllTicketsPage() {
		viewAllTicketsPage = new ViewAllTicketsPage(DriverManager.getDriver());
		viewAllTicketsPage.loadPage(ConfigUtil.getInstance().getConfig().getURL() + VIEW_ALL_TICKETS_PAGE_URL_PART);
		DriverManager.getDriver().navigate().refresh();
		viewAllTicketsPage.waitForTicketsToLoad();
	}
	
	public void examineTicketsTable() {
		Assert.assertTrue(viewAllTicketsPage.ticketsTableIsDisplayed());
	}
	
	public void verifyTicketsTableHasAtLeastOneColumn() {
		Assert.assertTrue(viewAllTicketsPage.ticketsTableHasAtLeastOneColumn());
	}
	
	public void verifyTicketsTableUIElementsAreDisplayed() {
		Assert.assertTrue(viewAllTicketsPage.ticketsTableUIElementsAreDisplayed());
	}
	
	public void sortByRandomColumn(String direction) {
		if (direction.equals(COLUMN_SORT_DIRECTION_DESC)) {
			sortingColumnIndex = viewAllTicketsPage.sortByRandomColumn(true);
		} else {
			sortingColumnIndex = viewAllTicketsPage.sortByRandomColumn(false);
		}
	}
	
	public void verifyTicketsTableDataSortedBy(String direction) {
		if (direction.equals(COLUMN_SORT_DIRECTION_DESC)) {
			Assert.assertTrue(viewAllTicketsPage.ticketsTableDataIsSortedBy(sortingColumnIndex, true));
		} else {
			Assert.assertTrue(viewAllTicketsPage.ticketsTableDataIsSortedBy(sortingColumnIndex, false));
		}
	}
	
	public void changeRowsPerPageTo(int rowsPerPage) {
		viewAllTicketsPage.changeRowsPerPage(rowsPerPage);
	}
	
	public void verifyNumberOfTicketsDisplayedEquals(int rowsPerPage) {
		Assert.assertTrue(viewAllTicketsPage.ticketsTableRowsEquals(rowsPerPage));
	}
	
	public void paginateToPageAndCaptureTickets(String pageNumber) {
		viewAllTicketsPage.paginateToPage(pageNumber);
		currentTicketsInView = viewAllTicketsPage.captureCurrentTicketsInView();
	}
	
	public void paginateToPage(String pageNumber) {
		viewAllTicketsPage.paginateToPage(pageNumber);
	}
	
	public void verifyThatTicketsDisplayedAreDifferent() {
		Assert.assertFalse(viewAllTicketsPage.ticketsDisplayedMatch(currentTicketsInView));
	}
	
	public void verifyThatPaginationButtonStatusesMatch(String listOfPages, String listOfStatuses) {
		Assert.assertTrue(viewAllTicketsPage.paginationButtonDisplayStatusesMatch(listOfPages, listOfStatuses));
	}
	
	public void clickRandomTicketRow() {
		int randomTicketNumber = viewAllTicketsPage.clickRandomTicketRow();
		try {
			TestFactoryUtils.storeTestObject("RANDOM_TICKET_NUMBER", randomTicketNumber);
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void clickTicketsTableConfigurationButton() {
		try {
			TestFactoryUtils.storeTestObject("CURRENT_COLUMN_CONFIG", viewAllTicketsPage.getCurrentTableColumnConfig());
			viewAllTicketsPage.clickTableConfigButton();
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void verifyTicketsTableConfigurationViewIsDisplayed() {
		Assert.assertTrue(viewAllTicketsPage.ticketsTableConfigViewIsDisplayed());
	}
	
	public void verifyTicketsTableConfigurationViewIsHidden() {
		Assert.assertFalse(viewAllTicketsPage.ticketsTableConfigViewIsDisplayed());
	}
	
	public void verifyTicketsTableConfigMatches(String matchStatus) {
		try {
			List<String> currentColumnConfig = TestFactoryUtils.getListStringTestObject("CURRENT_COLUMN_CONFIG");
			List<String> visibleColumnConfig = viewAllTicketsPage.getCurrentTableColumnConfig();
			if (matchStatus.equalsIgnoreCase("matches")) {
				Assert.assertTrue(currentColumnConfig.equals(visibleColumnConfig));
			} else if (matchStatus.equalsIgnoreCase("does not match")) {
				Assert.assertFalse(currentColumnConfig.equals(visibleColumnConfig));
			} else {
				Assert.fail();
			}
		} catch (Exception e) {
			Assert.fail();
		}
			
	}

}
