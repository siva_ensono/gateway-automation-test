package com.ensono.gateway.test.tests.home;

import org.testng.Assert;

import com.ensono.autoframework.utils.ConfigUtil;
import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.home.HomePage;
import com.ensono.gateway.page.navigation.NavigationPage;

public class HomeTest {
	
	private HomePage homePage;
	private NavigationPage navPage;
	
	public void loadGatewayApp() {
		navPage = new NavigationPage(DriverManager.getDriver());
		homePage = new HomePage(DriverManager.getDriver());
		homePage.loadPage(ConfigUtil.getInstance().getConfig().getURL());
		DriverManager.getDriver().navigate().refresh();
	}
	
	public void clickHomeLink() {
		navPage.clickHomeLink();
	}
	
	public void verifyPageTitle(String title) {
		Assert.assertTrue(homePage.pageTitleMatches(title));
	}
}
