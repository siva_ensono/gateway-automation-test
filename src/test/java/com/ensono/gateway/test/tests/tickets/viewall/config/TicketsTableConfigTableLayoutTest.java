package com.ensono.gateway.test.tests.tickets.viewall.config;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import com.ensono.autoframework.constants.Browsers;
import com.ensono.autoframework.utils.DriverManager;
import com.ensono.gateway.page.tickets.viewall.config.TicketsTableConfigTableLayoutPage;
import com.ensono.gateway.utils.TestFactoryUtils;

import junit.framework.Test;

public class TicketsTableConfigTableLayoutTest {
	
	private TicketsTableConfigTableLayoutPage ticketsTableConfigTableLayoutPage = new TicketsTableConfigTableLayoutPage(DriverManager.getDriver());
	
	public void verifyAtLeastColumnInGroup(int numberOfColumns, String fromGroupName) {
		int columnsInGroup = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup(fromGroupName);
		if (numberOfColumns > columnsInGroup) {
			ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(numberOfColumns - columnsInGroup, fromGroupName);
		}
	}
	
	public void moveNumberOfColumns(int numberOfColumns, String groupName){
		int columnsInGroup = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup(groupName);
		if (numberOfColumns > columnsInGroup) {
			ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(numberOfColumns - columnsInGroup, groupName);
		} else if(numberOfColumns < columnsInGroup) {
			ticketsTableConfigTableLayoutPage.removeColumnsFromGroup(Math.abs(numberOfColumns - columnsInGroup), groupName);
		}
		columnsInGroup = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup(groupName);
		Assert.assertTrue(numberOfColumns == columnsInGroup);
	}
	
	public void moveOneColumnToGroup(String action, String fromGroupName) {
		String movedColumnName = null;
		if (action.equalsIgnoreCase("add")) {
			movedColumnName = ticketsTableConfigTableLayoutPage.quickAddRandomColumn();
		} else {
			movedColumnName = ticketsTableConfigTableLayoutPage.quickRemoveRandomColumn();
		}
		try {
			TestFactoryUtils.storeTestObject("MOVED_COLUMN_NAME", movedColumnName);
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void verifyColumnWasMovedToGroup(String toGroupName) {
		String movedColumnName = null;
		try {
			movedColumnName = TestFactoryUtils.getStringTestObject("MOVED_COLUMN_NAME");
			Assert.assertTrue(ticketsTableConfigTableLayoutPage.columnExistsInGroup(movedColumnName, toGroupName));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void verifyColumnIsSortedInGroup(String sortValue, String groupName) {
		String movedColumnName = null;
		try {
			movedColumnName = TestFactoryUtils.getStringTestObject("MOVED_COLUMN_NAME");
			Assert.assertTrue(ticketsTableConfigTableLayoutPage.columnOrderedByInList(movedColumnName, groupName, sortValue));
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void dragAndDropColumn(String fromGroupName, String toGroupName) {
		String movedColumn = ticketsTableConfigTableLayoutPage.dragAndDropRandomColumn(fromGroupName, toGroupName);
		try {
			TestFactoryUtils.storeTestObject("MOVED_COLUMN_NAME", movedColumn);
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	public void moveAllColumnsToActive() {
		int columnsInAvailable = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup("available");
		ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(columnsInAvailable, "active");
	}
	
	public void changeColumnConfig() {
		int columnsInGroup = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup("available");
		if (columnsInGroup > 0) {
			ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(1, "active");
		} else {
			ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(1, "available");
		}
	}
	
	public void reorderColumnsInGroup(String groupName) {
		if (groupName.equalsIgnoreCase("active") || groupName.equalsIgnoreCase("available")) {
			ticketsTableConfigTableLayoutPage.reorderColumnsInGroup(groupName);
		} else {
			Assert.fail();
		}
	}
	
	public void moveAllButOneColumnToAvailable() {
		int columnsInActive = ticketsTableConfigTableLayoutPage.numberOfColumnsInGroup("active");
		ticketsTableConfigTableLayoutPage.moveNumberOfColumnsToGroup(columnsInActive - 1, "available");
	}
	
	public void clickOnOnlyColumnInActive() {
		ticketsTableConfigTableLayoutPage.clickRandomColumnInGroup("active");
	}
	
	public void verifyCannotRemoveColumnWarningIsDisplayed() {
		Assert.assertTrue(ticketsTableConfigTableLayoutPage.cannotRemoveColumnWarningIsDisplayed());
	}
	
	public void verifyCannotRemoveColumnWarningIsNotDraggable() {
		Assert.assertFalse(ticketsTableConfigTableLayoutPage.cannotRemoveColumnWarningIsDraggable());
	}
	
	public void verifyOnlyColumnInActiveIsNotDraggable() {
		Assert.assertFalse(ticketsTableConfigTableLayoutPage.lastActiveColumnIsDraggable());
	}
	
	public void verifyColumnsDoesNotHaveQuickActionButton(String groupName,String action) {
		Assert.assertFalse(ticketsTableConfigTableLayoutPage.columnsHaveQuickActionButton(groupName, action));
		
	}
	
	public void filterAvailableColumns() {
		try {
			String filterText = ticketsTableConfigTableLayoutPage.filterAvailableColumns();
			TestFactoryUtils.storeTestObject("CURRENT_FILTER_TEXT", filterText);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	public void verifyAvailableColumnsFilteredCorrectly() {
		try {
			String filterText = TestFactoryUtils.getStringTestObject("CURRENT_FILTER_TEXT");
			Assert.assertTrue(ticketsTableConfigTableLayoutPage.availableColumnsFilteredCorrectly(filterText));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	public void verifyClearFilterTextButtonIsDisplayed() {
		Assert.assertTrue(ticketsTableConfigTableLayoutPage.clearFilterTextButtonIsDisplayed());
	}
	
	public void clickClearFilterTextButton() {
		String filterText = ticketsTableConfigTableLayoutPage.clickClearFilterTextButton();
		try {
			TestFactoryUtils.storeTestObject("CURRENT_FILTER_TEXT", filterText);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	public void verifyClearFilterTextButtonIsHidden() {
		Assert.assertFalse(ticketsTableConfigTableLayoutPage.clearFilterTextButtonIsDisplayed());
	}
	
	public void verifyFilterTextInputIsEmpty() {
		Assert.assertTrue(ticketsTableConfigTableLayoutPage.filterTextInputIsEmpty());
	}
	
	public void verifyMaximunActiveColumnsWarningIsDisplayed() {
		Assert.assertTrue(ticketsTableConfigTableLayoutPage.cannotRemoveColumnWarningIsDisplayed());
	}

}
