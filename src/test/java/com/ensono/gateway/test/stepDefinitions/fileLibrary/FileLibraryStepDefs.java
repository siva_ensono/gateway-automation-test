package com.ensono.gateway.test.stepDefinitions.fileLibrary;

import com.ensono.gateway.test.tests.fileLibrary.FileLibraryTest;
import com.ensono.gateway.test.tests.home.HomeTest;

import cucumber.api.java.en.*;

public class FileLibraryStepDefs {
	
	private FileLibraryTest fileLibraryTest = new FileLibraryTest();

	private HomeTest homeTest = new HomeTest();
	
	@Given("^the FileLibrary module is opened$")
	public void open_file_library() {
		homeTest.loadGatewayApp();
		fileLibraryTest.openFileLibrary();
	}
	
	@When("^I open a random folder from '(.*)'$")
	public void open_random_folder(String FLView) {
		if(FLView.trim().toLowerCase().equals("tree")) {
			fileLibraryTest.openRandomFolderFromTree();
		} else if(FLView.trim().toLowerCase().equals("list")) {
			fileLibraryTest.openRandomFolderFromList();
		}
	}
	
	@Then("^I see the contents of the folder$")
	public void verify_folder_contents() {
		fileLibraryTest.verifyFolderContents();
	}

	
	@When ("^I click a breadcrumb link$")
	public void i_click_a_breadcrumb() {
		fileLibraryTest.openRandomFolderFromBreadcrumbs();
	}
	

}
