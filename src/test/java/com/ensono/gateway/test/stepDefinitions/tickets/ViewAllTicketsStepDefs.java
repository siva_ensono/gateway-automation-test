package com.ensono.gateway.test.stepDefinitions.tickets;

import com.ensono.gateway.test.tests.tickets.ViewAllTicketsTest;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ViewAllTicketsStepDefs {
	
	private ViewAllTicketsTest viewAllTicketsTest = new ViewAllTicketsTest();
	
	@Given("^I am on the view all tickets page$")
	public void i_am_on_the_view_all_tickets_page() {
		viewAllTicketsTest.loadViewAllTicketsPage();
	}
	
	@When("^I examine the tickets table$")
	public void i_examine_the_tickets_table() {
		viewAllTicketsTest.examineTicketsTable();
	}
	
	@Then("^the tickets table has at least one column$")
	public void the_tickets_table_has_at_least_one_column() {
		viewAllTicketsTest.verifyTicketsTableHasAtLeastOneColumn();
	}
	
	@Then("^all tickets table UI elements are displayed$")
	public void all_tickets_table_ui_elements_are_displayed() {
		viewAllTicketsTest.verifyTicketsTableUIElementsAreDisplayed();
	}
	
	@When("^I sort by random ticket column '(.*)'$")
	public void i_sort_by_random_ticket_column(String direction) {
		viewAllTicketsTest.sortByRandomColumn(direction);
	}
	
	@Then("^the data is ordered by that column '(.*)'$")
	public void the_data_is_ordered_by_that_column(String direction) {
		viewAllTicketsTest.verifyTicketsTableDataSortedBy(direction);
	}
	
	@When("^I change the rows per page to '(\\d+)'$")
	public void i_change_the_rows_per_page_to(int rowsPerPage) {
		viewAllTicketsTest.changeRowsPerPageTo(rowsPerPage);
	}
	
	@Then("^'(\\d+)' rows are displayed in the tickets table$")
	public void rows_are_displayed_in_the_tickets_table(int rowsPerPage) {
		viewAllTicketsTest.verifyNumberOfTicketsDisplayedEquals(rowsPerPage);
	}
	
	@Given("^I am on table page '(.*)'$")
	public void i_am_on_table_page(String pageButton) {
		viewAllTicketsTest.paginateToPageAndCaptureTickets(pageButton);
	}
	
	@When("^I click the pagination button labeled '(.*)'$")
	public void i_click_the_pagination_button_labeled(String pageButton) {
		viewAllTicketsTest.paginateToPage(pageButton);
	}
	
	@Then("^the table rows displayed should be different$")
	public void the_table_rows_displayed_should_be_different() {
		viewAllTicketsTest.verifyThatTicketsDisplayedAreDifferent();
	}
	
	@Then("^the enabled status of '(.*)' should be '(.*)'$")
	public void the_enabled_statuses_should_be(String listOfPages, String listOfStatuses) {
		viewAllTicketsTest.verifyThatPaginationButtonStatusesMatch(listOfPages, listOfStatuses);
	}
	
	@When("^I click a random ticket$")
	public void i_click_a_random_ticket() {
		viewAllTicketsTest.clickRandomTicketRow();
	}
	
	@When("^I click on the tickets table configuration button$")
	public void i_click_on_the_tickets_table_configuration_button() {
		viewAllTicketsTest.clickTicketsTableConfigurationButton();
	}
	
	@Then("^the tickets table configuration view is displayed$")
	public void the_tickets_table_configuration_view_is_displayed() {
		viewAllTicketsTest.verifyTicketsTableConfigurationViewIsDisplayed();
	}
	
	@Then("^the tickets table configuration view is hidden$")
	public void the_tickets_table_configuration_view_is_hidden() {
		viewAllTicketsTest.verifyTicketsTableConfigurationViewIsHidden();
	}
	
	@Then("^the tickets table configuration '(?:matches|does not match)'$")
	public void the_tickets_table_configuration(String matchStatus) {
		viewAllTicketsTest.verifyTicketsTableConfigMatches(matchStatus);
	}

}
