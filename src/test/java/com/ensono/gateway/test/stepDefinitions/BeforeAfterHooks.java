package com.ensono.gateway.test.stepDefinitions;

import java.util.logging.Logger;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;

import com.ensono.autoframework.constants.Browsers;
import com.ensono.autoframework.utils.DriverManager;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class BeforeAfterHooks {
	
	private static final Logger LOGGER = Logger.getLogger(BeforeAfterHooks.class.getName());
    
    /**
     * Delete all cookies at the start of each scenario to avoid
     * shared state between tests
     */
    @Before
    public void deleteAllCookies(Scenario scenario) {        
        LOGGER.info("Deleting all cookies...");        
        
        WebDriver driver = DriverManager.getDriver();
        driver.manage().deleteAllCookies();
        
        String browserName = DriverManager.getBrowserInfo();
        
        if (browserName.contains(Browsers.FIREFOX.toString().toLowerCase())) {
        	driver.get("javascript:localStorage.clear();");
        } else if (browserName.contains(Browsers.CHROME.toString().toLowerCase())) {
        	if(!driver.getCurrentUrl().contains("data:")){
        		LOGGER.info("Entered url condition");
        //		LocalStorage local = ((WebStorage) driver).getLocalStorage();
        //		local.clear();
        	}
        } else {
        	((JavascriptExecutor) driver).executeScript("localStorage.clear();");
        	((JavascriptExecutor) driver).executeScript("sessionStorage.clear();");
        }
       
//	    if (DriverManager.getBrowserInfo().contains(Browsers.CHROME.toString())) {
//    		Collection<String> tags = scenario.getSourceTagNames();
//	    	for (String tag : tags) {
//	    		if (tag.contains("@CHROMESKIP")) {
//	    			// HOW TO SKIP SCENARIO?
//	    		}
//	    	}
//	    }
    }

    /**
     * Embed a screenshot in test report if test is marked as failed
     */
    @After
    public static void embedScreenshot(Scenario scenario) {
    	
        if ( scenario.isFailed() ) {
        	LOGGER.info("Scenario failed! Browser: " + DriverManager.getDriver() + " Taking screenshot...");
        	scenario.write("Current Page URL is: " + DriverManager.getDriver().getCurrentUrl());        	
            scenario.write("Scenario Failed in: " + DriverManager.getBrowserInfo());
            try {
                  byte[] screenshot = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
                  scenario.embed(screenshot, "target/image");                  
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
            	LOGGER.info(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
    }

}
