package com.ensono.gateway.test.stepDefinitions.navigation;

import com.ensono.gateway.test.tests.navigation.NavigationEditorTest;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NavigationEditorStepDefs {
	
	NavigationEditorTest navEditTest = new NavigationEditorTest();
	
	@Given ("^I have disabled a module that was previously enabled$")
	public void i_have_disabled_enabled_module() {
		navEditTest.disableRandomActiveModule();
	}
	
	@Given ("^I am on the tickets module$")
	public void i_am_on_the_tickets_module() {
		navEditTest.loadTicketsModule();
	}
	
	@When("^I click the Manage Modules button$")
	public void i_click_the_manage_modules_button() {
		navEditTest.clickManageModules();
	}
	
	@When("^I click the cancel button$")
	public void i_click_the_cancel_button() {
		navEditTest.clickCancelButton();
	}
	
	@When ("^I '(.*)' a random module$")
	public void i_change_a_random_module(String action) {
		navEditTest.toggleRandomModule(action);
	}
	
	@When ("^I disable the '(.*)' module$")
	public void i_disable_the_module(String module) { 
		navEditTest.disableModule(module);
	}
	
	@When("^I enable the '(.*)' module$")
	public void i_enable_the_module(String module) {
		navEditTest.enableModule(module);
	}
	
	@And("^there is at least one module '(.*)'$")
	public void there_is_at_least_one_module(String state) {
		navEditTest.guarenteeOneModule(state);
	}
	
	@And ("^I click the save button$")
	public void i_click_the_save_button() {
		navEditTest.clickSaveButton();
	}
			
	@Then("^the navigation editor will appear$")
	public void the_navigation_editor_will_appear() {
		navEditTest.verifyEditorIsVisible(true);
	}
	
	@Then("^the navigation editor will disappear$")
	public void the_navigation_editor_will_disappear() {
		navEditTest.verifyEditorIsVisible(false);
	}
	
	@Then("^the module (?:will|will not) show up in the navigation sidebar$")
	public void the_module_will_or_will_not_show_up() {
		navEditTest.verifyActiveModules();
	}
	
	@Then ("^I will be redirected to the home page$")
	public void i_will_be_redirected() {
		navEditTest.verifyCurrentPage("Home");
	}
	
	@Then("^the tooltip should be visible$")
	public void the_tooltip_should_be_visible() {
		navEditTest.verifyTooltipIsVisible();
	}
	
	@Then("^the tooltip should not be visible$")
	public void the_tooltip_should_be_invisible() {
		navEditTest.verifyTooltipIsInvisible();
	}
	
	@Then("^the backdrop should be visible$")
	public void the_backdrop_should_be_visible() {
		navEditTest.verifyBackdropIsVisible();
	}
	

}
