package com.ensono.gateway.test.stepDefinitions.home;

import com.ensono.gateway.test.tests.home.HomeTest;

import cucumber.api.java.en.*;

public class HomeStepDefs {
	
	private HomeTest homeTest = new HomeTest();
	
	@Given("^the gateway application is loaded$")
	public void load_gateway() {
		homeTest.loadGatewayApp();
	}

	@When("^I click on the home navigation link$")
	public void click_home_navigation_link() {
		homeTest.clickHomeLink();
	}
	
	@Then("^I should be on the '(.*)' page$")
	public void i_should_be_on_the_landing_page(String title) {
		homeTest.verifyPageTitle(title);
	}
	
}
