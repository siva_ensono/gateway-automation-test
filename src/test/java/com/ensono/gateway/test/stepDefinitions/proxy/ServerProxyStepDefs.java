package com.ensono.gateway.test.stepDefinitions.proxy;


import java.util.logging.Logger;
import com.ensono.autoframework.utils.ProxyServer;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ServerProxyStepDefs {
	private static final Logger LOGGER = Logger.getLogger(ServerProxyStepDefs.class.getName());	
	private String requestInterceptor;
	
	@Given("^http request is intercepted$")
	public void i_load_the_url() {
		LOGGER.info("request is intercepted ");
	}

	@When("^request end point matches with '(.*)'$")
	public void request_end_point_matches_with(String requestInterceptor) {			
		this.requestInterceptor = requestInterceptor;
	}
	
	@Then("^response received with '(.*)'$")
	public void the_modified_page_should_be_displayed(String messageContents) {
		ProxyServer.addProxyListeners(requestInterceptor, messageContents);	
	}	
	
}
