package com.ensono.gateway.test.stepDefinitions.tickets.viewall;

import com.ensono.gateway.test.tests.tickets.viewall.TicketsTableConfigTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TicketsTableConfigStepDefs {
	
	private TicketsTableConfigTest ticketsTableConfigTest = new TicketsTableConfigTest();
	
	@When("^I click on the '(.*)' config tab$")
	public void i_click_on_the_config_tab(String tabName) {
		ticketsTableConfigTest.clickOnConfigTab(tabName);
	}
	
	@Then("^the '(.*)' tab is displayed$")
	public void the_config_tab_is_displayed(String tabName) {
		ticketsTableConfigTest.verifyConfigTabIsDisplayed(tabName);
	}
	
	@When("^I click the tickets table config '(.*)' button$")
	public void i_click_the_tickets_table_config_button(String buttonType) {
		ticketsTableConfigTest.clickConfigDialogActionButton(buttonType);
	}
	
	@Then("^the resize message is displayed$")
	public void the_resize_message_is_displayed() { 
		ticketsTableConfigTest.verifyResizeMessageDisplayed();
	}

}
