package com.ensono.gateway.test.stepDefinitions.tickets.viewall.config;

import com.ensono.gateway.test.tests.tickets.viewall.config.TicketsTableConfigTableLayoutTest;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TicketsTableConfigTableLayoutStepDefs {

	private TicketsTableConfigTableLayoutTest ticketsTableConfigTableLayoutTest = new TicketsTableConfigTableLayoutTest();
	
	@Given("^there is at least '(\\d+)' (?:column|columns) in '(.*)'$")
	public void there_is_at_least_column_in(int numberOfColumns, String fromGroupName) {
		ticketsTableConfigTableLayoutTest.verifyAtLeastColumnInGroup(numberOfColumns, fromGroupName);
	}
	
	@Given("^there (?:is|are) '(\\d+)' (?:column|columns) in '(.*)'$")
	public void there_are_columns_in(int numberOfColumns, String groupName) {
		ticketsTableConfigTableLayoutTest.moveNumberOfColumns(numberOfColumns, groupName);
	}
	
	@When("^I click the quick '(.*)' button on a random column in '(.*)'$")
	public void i_click_the_quick_button_on_a_random_column_in(String action, String fromGroupName) {
		ticketsTableConfigTableLayoutTest.moveOneColumnToGroup(action, fromGroupName);
	}
	
	@Then("^that column is moved to '(.*)'$")
	public void that_column_is_moved_to(String toGroupName) {
		ticketsTableConfigTableLayoutTest.verifyColumnWasMovedToGroup(toGroupName);
	}
	
	@Then("^that column is sorted '(.*)' in group '(.*)'$")
	public void that_column_is_sorted_in_group(String sortValue, String toGroupName) {
		ticketsTableConfigTableLayoutTest.verifyColumnIsSortedInGroup(sortValue, toGroupName);
	}
	
	@When("^I drag and drop a random column from '(.*)' into '(.*)'$")
	public void i_drag_and_drop_a_random_column_from_group_into_group(String fromGroupName, String toGroupName) {
		ticketsTableConfigTableLayoutTest.dragAndDropColumn(fromGroupName, toGroupName);
	}
	
	@When("^I move all columns to active$")
	public void i_move_all_columns_to_active() {
		ticketsTableConfigTableLayoutTest.moveAllColumnsToActive();
	}
	
	@When("^I change the table layout column configuration$")
	public void i_change_the_table_layout_column_configuration() {
		ticketsTableConfigTableLayoutTest.changeColumnConfig();
	}
	
	@When("^I reorder the columns in '(active|available)'$")
	public void i_reorder_the_columns_in(String groupName) {
		ticketsTableConfigTableLayoutTest.reorderColumnsInGroup(groupName);
	}
	
	@When("^I move all but one column to available$")
	public void i_move_all_but_one_column_to_available() {
		ticketsTableConfigTableLayoutTest.moveAllButOneColumnToAvailable();
	}
	
	@When("^I click on the only column in active$")
	public void i_click_on_the_only_column_in_active() {
		ticketsTableConfigTableLayoutTest.clickOnOnlyColumnInActive();
	}
	
	@Then("^the cannot remove column warning will appear$")
	public void the_cannot_remove_column_warning_will_appear() {
		ticketsTableConfigTableLayoutTest.verifyCannotRemoveColumnWarningIsDisplayed();
	}
	
	@Then("^the cannot remove column warning is not draggable$")
	public void the_cannot_remove_column_warning_is_not_draggable() {
		ticketsTableConfigTableLayoutTest.verifyCannotRemoveColumnWarningIsNotDraggable();
	}
	
	@Then("^the only column in active is not draggable$")
	public void the_only_column_in_active_is_not_draggable() {
		ticketsTableConfigTableLayoutTest.verifyOnlyColumnInActiveIsNotDraggable();
	}
	
	@Then("^the (?:column|columns) in '(.*)' does not have a quick '(.*)' button$")
	public void the_column_does_not_have_button(String groupName, String action) {
		ticketsTableConfigTableLayoutTest.verifyColumnsDoesNotHaveQuickActionButton(groupName, action);
	}
	
	@When("^I filter the available columns$")
	public void i_filter_the_available_columns() {
		ticketsTableConfigTableLayoutTest.filterAvailableColumns();
	}
	
	@Then("^the available columns are filtered correctly$")
	public void the_available_columns_are_filtered_correctly() {
		ticketsTableConfigTableLayoutTest.verifyAvailableColumnsFilteredCorrectly();
	}
	
	@Then("^the clear filter text button is displayed$")
	public void the_clear_filter_text_button_is_displayed() {
		ticketsTableConfigTableLayoutTest.verifyClearFilterTextButtonIsDisplayed();
	}
	
	@When("^I click the clear filter text button$")
	public void i_click_the_clear_filter_text_button() {
		ticketsTableConfigTableLayoutTest.clickClearFilterTextButton();
	}
	
	@Then("^the clear filter text button is hidden$")
	public void the_clear_filter_text_button_is_hidden() {
		ticketsTableConfigTableLayoutTest.verifyClearFilterTextButtonIsHidden();
	}
	
	@Then("^the filter text input is empty$")
	public void the_filter_text_input_is_empty() {
		ticketsTableConfigTableLayoutTest.verifyFilterTextInputIsEmpty();
	}
	
	@Then("^the cannot have more than 10 active columns warning will appear$")
	public void the_cannot_have_more_than_10_active_columns_warning_will_appear() {
		ticketsTableConfigTableLayoutTest.verifyMaximunActiveColumnsWarningIsDisplayed();
	}
	
}
