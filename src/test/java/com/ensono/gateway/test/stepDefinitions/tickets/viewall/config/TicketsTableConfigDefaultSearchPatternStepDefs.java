package com.ensono.gateway.test.stepDefinitions.tickets.viewall.config;

import com.ensono.gateway.test.tests.tickets.viewall.config.TicketsTableConfigDefaultSearchPatternTest;

import cucumber.api.java.en.Then;

public class TicketsTableConfigDefaultSearchPatternStepDefs {

	private TicketsTableConfigDefaultSearchPatternTest ticketsTableConfigDefaultSearchPatternTest = new TicketsTableConfigDefaultSearchPatternTest();
	
	@Then("^the refresh rate selector is displayed$")
	public void the_refresh_rate_selector_is_displayed() {
		ticketsTableConfigDefaultSearchPatternTest.checkRefreshIntervalSelectorIsDisplayed();
	}
	
	@Then("^the refresh rate selector is defaulted to 60 seconds$")
	public void the_refresh_rate_selector_is_defaulted_to_60_seconds() {
		ticketsTableConfigDefaultSearchPatternTest.checkRefreshDefaultsToSixty();
	}
	
}
