package com.ensono.gateway.test.stepDefinitions.notifications;

import com.ensono.gateway.test.tests.notifications.NotificationsTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NotificationsStepDefs {
	
	NotificationsTest notificationsTest = new NotificationsTest();
	
	@Then("^the notifications panel is displayed$")
	public void the_notifications_panel_is_displayed() {
		notificationsTest.isNotificationPanelDisplayed(true);
	}
	
	@Then("^the notifications panel is (?:closed|not displayed)$")
	public void the_notifications_panel_is_closed() {
		notificationsTest.isNotificationPanelDisplayed(false);
	}
		
	@When("^I click outside the notifications panel$")
	public void i_click_outside_the_notifications_panel() {
		notificationsTest.clickOutsideNotificationsPanel();
	}
	
	

}
