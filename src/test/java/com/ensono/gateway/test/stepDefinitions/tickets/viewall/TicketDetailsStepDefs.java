package com.ensono.gateway.test.stepDefinitions.tickets.viewall;

import com.ensono.gateway.test.tests.tickets.viewall.TicketDetailsTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TicketDetailsStepDefs {

	private TicketDetailsTest ticketDetailsTest = new TicketDetailsTest();
	
	@Then("^the ticket details view is displayed$")
	public void the_ticket_details_view_is_displayed() {
		ticketDetailsTest.verifyTicketDetailsViewIsDisplayed();
	}
	
	@Then("^the ticket details view matches that ticket$")
	public void the_ticket_details_view_matches_that_ticket() {
		ticketDetailsTest.verifyTicketDetailsNumberMatches();
	}
	
	@When("^I click the ticket details view close button$")
	public void i_click_the_ticket_details_view_close_button() {
		ticketDetailsTest.closeTicketsDetailView();
	}
	
	@Then("^the ticket details view is closed$")
	public void the_ticket_details_view_is_closed() {
		ticketDetailsTest.verifyTicketDetailsViewIsClosed();
	}
	
}
