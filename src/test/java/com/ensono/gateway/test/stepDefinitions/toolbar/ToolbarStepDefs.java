package com.ensono.gateway.test.stepDefinitions.toolbar;

import com.ensono.gateway.test.tests.notifications.NotificationsTest;
import com.ensono.gateway.test.tests.toolbar.ToolbarTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ToolbarStepDefs {

	ToolbarTest toolbarTest = new ToolbarTest();
	NotificationsTest notificationsTest = new NotificationsTest();
	
	@When("^I click notifications button$")
	public void i_click_notifications_button() {
		toolbarTest.clickNotificationsButton();
		notificationsTest.isNotificationPanelDisplayed(true);
	}
	
	@Then("^notification button is not clickable$")
	public void notification_button_is_not_clickable() {
		toolbarTest.verifyNotificationsButtonIsNotClickable();
	}
	
}
